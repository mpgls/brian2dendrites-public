{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1 style=\"color:white; background-color:#008080; text-align:center; height:60px; padding: 20px\">\n",
    "    Brian2dendrites\n",
    "</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Description\n",
    "<p style=\"text-align:justify\">\n",
    "Brian2dendrites is a python package that is designed to simplify the development of reduced compartmental neuron models in Brian2. It automatically generates and handles all the differential equations needed to describe dendritic segments, along with the various currents passing through them. Below a realistic workflow of modeling a simplified pyramidal neuron with dendrites is provided.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 1: Make all necessary imports\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from brian2 import *\n",
    "from brian2_dendrites import Compartment, NeuronModel, library\n",
    "from active_dendrites_params import pyramidal_params"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note**: The way parameters are passed into this model will be explained later on in more detail."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 2: Create some compartments\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "soma  = Compartment(tag='soma', model='leakyIF', neuron=\"pyramidal\")\n",
    "trunk = Compartment(tag='trunk', model='passive', neuron=\"pyramidal\")\n",
    "proximal  = Compartment(tag='prox', model='passive', neuron=\"pyramidal\")\n",
    "distal  = Compartment(tag='dist', model='passive', neuron=\"pyramidal\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each ***Compartment*** object automatically generates a set of equation strings based on the model selected (leakyIF = leaky I&F, passive = leaky integrator). The ***tag*** kwarg must be a unique name that will help to distinguish between the various compartments belonging to a single neuron. The ***neuron*** kwarg is used to group compartments of the same neuron (useful when consturcting networks consisting of several neuron types)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Tip**: To get useful information regarding a compartment, one can simply print it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(soma)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also all of its attributes can be accessed using the dot (.) notation as shown below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(soma.eqs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 3: Connect the compartments\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trunk.connect_to(soma)\n",
    "proximal.connect_to(trunk)\n",
    "distal.connect_to(proximal)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The previous lines of code allow the electrical coupling of adjacent compartments. Notice how the equations of each compartment have automatically been updated to include the coupling currents."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(distal.eqs, end='\\n\\n')\n",
    "print(proximal.eqs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 4: Add synaptic current equations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "distal.add_synapse(type='ampa', source='region1')\n",
    "distal.add_synapse(type='nmda', source='region1')\n",
    "proximal.add_synapse(type='ampa', source='region2')\n",
    "proximal.add_synapse(type='nmda', source='region2')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Synaptic current equations can be directly added to a given compartment throught the ***add_synapse()*** method. The ***type*** kwarg is for choosing ion channel type (options: 'ampa', 'nmda', 'gaba') and the \"source\" kwarg is used to separate synapses of the same channel type coming from different sources. Notice how the model equations have been updated again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(distal.eqs, end='\\n\\n')\n",
    "print(proximal.eqs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 4: Activate dendritic spikes mechanism"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trunk.add_dspikes()\n",
    "proximal.add_dspikes()\n",
    "distal.add_dspikes()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we continue, let's take a look at what changes the add_dspikes method caused to a compartment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "print(distal)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dendritic spiking is recreated phenomenologically (without using HH equations) by taking advantage of Brian's custom events support. The rising phase of dspikes is due to \"INa\" current injections when Vdend > Vth_dspike. The decay phase is due to \"IK\" currents that are generated shortly after \"INa\" currents are produced. By adjusting tau_Na, tau_K, INa_max, and IK_max it is possible to adjust the amplitude, shape, and duration of dspikes produced. Also, a refractory mechanism has been added to ensure that dspike firing is more realistic."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 5: Merge all the compartments into a single neuron model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"image.png\" style=\"width: 150px;\" />"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyr_model = NeuronModel(neuron='pyramidal', namespace=pyramidal_params)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ***neuron*** kwarg is used to detect all compartments that have been tagged with the same neuron name. The ***namespace*** kwarg is used to pass all necessary model parameters as a dictionary. Let's now see how a NeuronModel looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(pyr_model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we see, even a relatively simple multicompartmental model requires a fair amount of differential equations, parameters and events to be described. As the number of compartments, neuron types and synaptic pathways increases, maintaining such a complex set of objects becomes increasingly difficult and error-prone. By automating this process through Brian2dendrites, compartmental modeling becomes easier and more time-efficient."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 6: Create and initialize a NeuronGroup using NeuronModel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyr_group = NeuronGroup(3, model=pyr_model.equations, threshold='V_soma>Vth_soma', \n",
    "                        method='euler', reset ='V_soma = 40*mV', refractory=4*ms, \n",
    "                        events = pyr_model.events, name='pyr_group', \n",
    "                        namespace=pyramidal_params)\n",
    "\n",
    "# Create a double reset mechanism for more realistic somatic firing\n",
    "second_reset = Synapses(pyr_group, pyr_group, on_pre='V_soma=-55*mV', delay=0.5*ms)\n",
    "second_reset.connect(j='i')\n",
    "\n",
    "# Initialize V rest and dspikes mechanism\n",
    "pyr_model.bind(pyr_group, scope=globals())  # To unlock the automatons used in the folowing 2 lines\n",
    "pyr_model.set_rest()\n",
    "pyr_model.handle_dspikes()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note 1:** The reason we use this second reset will be made clear shortly after.<br>\n",
    "**Note 2:** The set_rest() and handle_dspikes() methods create executable code that initializes Vrest across all compartments and also activates the dspikes mechanism. Setting debug=True just prints the code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyr_model.set_rest(debug=True)\n",
    "pyr_model.handle_dspikes(debug=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 7: Create synaptic input and run simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 12\n",
    "isi = 4    # ms\n",
    "seq_input1 = SpikeGeneratorGroup(N, range(N), range(100, 100+(N*isi), isi)*ms)\n",
    "S_region1 = Synapses(seq_input1, pyr_group, on_pre=\"s_ampa_region1_dist += 1.0; s_nmda_region1_dist += 1.0\")\n",
    "S_region1.connect(j='1')\n",
    "\n",
    "# Random region2 to distal dendrite\n",
    "seq_input2 = SpikeGeneratorGroup(N, range(N), range(100, 100+(N*isi), isi)*ms)\n",
    "S_region2 = Synapses(seq_input2, pyr_group, on_pre=\"s_ampa_region2_prox += 1.0; s_nmda_region2_prox += 1.0\")\n",
    "S_region2.connect(j='2')\n",
    "\n",
    "to_monitor = ('V_soma', 'V_prox', 'V_dist', 'V_trunk', 'Iampa_region1_dist', \n",
    "              'Inmda_region1_dist')\n",
    "M = StateMonitor(pyr_group, to_monitor, record=True)\n",
    "\n",
    "run(20*ms)\n",
    "pyr_group.Iinj_soma[0] = 130 * pA\n",
    "run(200*ms)\n",
    "pyr_group.Iinj_soma[0] = 0 * pA\n",
    "run(120*ms)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 8: Visualise output\n",
    "(The python coded needed to produce the following figure is not shown here for practical reasons but you can find it in the attached files.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"image2.png\" style=\"width: 600px;\" />"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**a)** Schematic illustration of the reduced neuron model, also showing the place of synapses. The thickness of each cylinder is representative of each compartment's \"diameter\".\n",
    "**b)** High-frequency synaptic activation at the distal dendritic compartment. Dendritic spikes are easily produced at the distal dendrite because of its smaller size. However, they do not propagate to the proximal compartment due to dendritic attenuation.\n",
    "**c)** Same input as in panel **a** but delivered at the proximal compartment. Less dendritic spikes are generated due to the larger size of the proximal dendrite, but they efficiently propagate towards both the distal and somatic compartments.\n",
    "**d)** Somatic current injection causing the generation of backpropagating action potentials. Notice how they deminish as they travel from the soma towards the distal dendrite."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**IMPORTANT NOTE** <br>\n",
    "The somatic spikes shown here are not manually drawn as usually happens in traces of I&F models. They are realistic APs caused by the double reset mechanism that was mentioned earlier. In compartmental I&F models, using the typical single-reset mechanism would result in unrealistic dendritic responses since they couldn't \"sense\" the somatic activity."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### And what about model parameters?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import pi\n",
    "from brian2.units import *\n",
    "from brian2_dendrites import Properties\n",
    "\n",
    "\n",
    "# General passive parameters:\n",
    "sf       =  2.8                     # scale factor to account for reduced model area   \n",
    "Cm_soma  =  1  * uF/(cm**2) * sf    # specific membrane capacitance\n",
    "gL_soma  =  25 * uS/(cm**2) * sf    # specific leakage conductance  \n",
    "Cm_dend  =  Cm_soma * 1.5           # to account for spines\n",
    "gL_dend  =  gL_soma * 1.5           # to account for spines\n",
    "Ra       =  150.0 * ohm * cm        # specific axial resistance\n",
    "\n",
    "# Create some Properties objects\n",
    "soma_params = Properties(length=30*um, diameter=30*um, cm=Cm_soma, gl=gL_soma, \n",
    "                         ra=Ra, Vrest=-70*mV, Vth=-40*mV, tag='soma')\n",
    "\n",
    "trunk_params = Properties(length=100*um, diameter=2.5*um, cm=Cm_dend, gl=gL_dend, \n",
    "                          ra=Ra, Vrest=-70*mV, Vth=-40*mV, tag='trunk')\n",
    "\n",
    "prox_params = Properties(length=150*um, diameter=2*um, cm=Cm_dend, gl=gL_dend, \n",
    "                         ra=Ra, Vrest=-70*mV, Vth=-40*mV, tag='prox')\n",
    "\n",
    "dist_params = Properties(length=150*um, diameter=1*um, cm=Cm_dend, gl=gL_dend, \n",
    "                         ra=Ra, Vrest=-70*mV, Vth=-40*mV, tag='dist')\n",
    "\n",
    "g_couples = {\n",
    "\"g_trunk_soma\"   :  trunk_params.g_cylinder,    # Using the normal g_couple results in very strong coupling\n",
    "\"g_prox_trunk\"   :  Properties.find_g_couple(trunk_params, prox_params),\n",
    "\"g_dist_prox\"    :  Properties.find_g_couple(dist_params, prox_params)\n",
    "}\n",
    "\n",
    "general_synaptic_params = {\n",
    "\"Egaba\"          :  -80   * mV,\n",
    "\"tGABA\"          :   10   * ms,\n",
    "\"Eampa\"          :   0    * mV,\n",
    "\"tAMPA\"          :   7    * ms,\n",
    "\"Enmda\"          :   0    * mV,\n",
    "\"tNMDA\"          :   70   * ms, \n",
    "\"gamma\"          :   0.062 * mV**-1,\n",
    "\"Mg\"             :   1,           \n",
    "\"eta\"            :   0.1\n",
    "}\n",
    "\n",
    "synaptic_params = {\n",
    "\"gAMPA_region1_dist\"  :   1  * nS,\n",
    "\"gNMDA_region1_dist\"  :   1  * nS,\n",
    "\"gAMPA_region2_prox\"  :   1  * nS,\n",
    "\"gNMDA_region2_prox\"  :   1  * nS\n",
    "}\n",
    "\n",
    "dspike_params = {\n",
    "\"INa_refractory\"   :  5  * ms,\n",
    "\"tau_Na\"           : 0.6 * ms,     # INa decay constant\n",
    "\"tau_K\"            : 0.6 * ms,     # IK decay constant\n",
    "# Current jumps at dendritic spike events:\n",
    "\"INa_dist_max\"    :   2900 * pA * (dist_params.area/prox_params.area),\n",
    "\"IK_dist_max\"     :  -3000 * pA * (dist_params.area/prox_params.area),\n",
    "\"INa_prox_max\"    :   2900 * pA,\n",
    "\"IK_prox_max\"     :  -3000 * pA,\n",
    "\"INa_trunk_max\"   :   2900 * pA * (trunk_params.area/prox_params.area),\n",
    "\"IK_trunk_max\"    :  -3000 * pA * (trunk_params.area/prox_params.area)\n",
    "}\n",
    "\n",
    "\n",
    "pyramidal_params = Properties.merge_params([soma_params, trunk_params, prox_params, \n",
    "                             dist_params, g_couples, general_synaptic_params,\n",
    "                             synaptic_params, dspike_params])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Comment** <br>\n",
    "The way I pass the parameters in the model is still under development. In general, I prefer to store all the parameters separately, so that the main program stays clear and readable. My approach is to first create all the necessary model equations and Brian objects using simple, Lego-style code, without worrying so much about model parameters. Then, using a formula I have developed (not described here but I can share it with you if you are interested), I pass all the parameters needed as a dictionary using the ***Properties*** class. During this step, I also perform extensive validation of the model to ensure it is as close to experimental data as possible. I have some ideas on how to further simplify the whole process but I am still working on them. I would be immensely grateful for any feedback or constructive criticism you could provide me with. Thank you."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
