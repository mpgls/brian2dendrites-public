from brian2.units import *

pyramidal_params = {
# Passive properties of soma:
"EL_soma":   -70 * mV,
"C_soma":    200 * pF,
"gL_soma":     4 * nS,
# Passive properties of apical dendrite:
"EL_apical": -70 * mV,
"C_apical":  100 * pF,
"gL_apical":   2 * nS,
# Passive properties of basal dendrite:
"EL_basal":  -70 * mV,
"C_basal":   100 * pF, 
"gL_basal":    2 * nS, 
# Conductance of coupling dendritic currents:
"g_soma_apical": 12 * nS,
"g_apical_soma": 12 * nS,
"g_soma_basal":  10 * nS,
"g_basal_soma":  10 * nS
}