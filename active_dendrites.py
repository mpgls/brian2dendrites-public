from brian2 import *
from brian2_dendrites import Compartment, NeuronModel
from active_dendrites_params import pyramidal_params

# Soma equations
soma  = Compartment(tag='soma', model='leakyIF', neuron="pyramidal")

# Trunk equations
trunk = Compartment(tag='trunk', model='passive', neuron="pyramidal")
trunk.add_dspikes()

# Proximal dendrite equations
proximal  = Compartment(tag='prox', model='passive', neuron="pyramidal")
proximal.add_synapse(type='ampa', source='region2')
proximal.add_synapse(type='nmda', source='region2')
proximal.add_dspikes()

# Distal dendrite equations
distal  = Compartment(tag='dist', model='passive', neuron="pyramidal")
distal.add_synapse(type='ampa', source='region1')
distal.add_synapse(type='nmda', source='region1')
distal.add_dspikes()

# Connect compartments
trunk.connect_to(soma)
proximal.connect_to(trunk)
distal.connect_to(proximal)

# Create neuronal model
pyr_model = NeuronModel(neuron='pyramidal', namespace=pyramidal_params)

# Create neuronal group
pyr_group = NeuronGroup(3, model=pyr_model.equations, threshold='V_soma>Vth_soma', 
                        method='euler', reset ='V_soma = 40*mV', refractory=4*ms, 
                        events = pyr_model.events, name='pyr_group', 
                        namespace=pyramidal_params)

# Double reset mechanism
second_reset = Synapses(pyr_group, pyr_group, on_pre='V_soma=-55*mV', delay=0.5*ms)
second_reset.connect(j='i')

# Initialize V rest and dspikes
pyr_model.bind(pyr_group, scope=globals())
pyr_model.set_rest()
pyr_model.handle_dspikes()

# Create sequantial input from region1 to distal dendrite
N = 12
isi = 4    # ms
seq_input1 = SpikeGeneratorGroup(N, range(N), range(100, 100+(N*isi), isi)*ms)
S_region1 = Synapses(seq_input1, pyr_group, on_pre="s_ampa_region1_dist += 1.0; s_nmda_region1_dist += 1.0")
S_region1.connect(j='1')

# Random region2 to distal dendrite
seq_input2 = SpikeGeneratorGroup(N, range(N), range(100, 100+(N*isi), isi)*ms)
S_region2 = Synapses(seq_input2, pyr_group, on_pre="s_ampa_region2_prox += 1.0; s_nmda_region2_prox += 1.0")
S_region2.connect(j='2')

to_monitor = ('V_soma', 'V_prox', 'V_dist', 'V_trunk', 'Iampa_region1_dist', 
              'Inmda_region1_dist')
M = StateMonitor(pyr_group, to_monitor, record=True)

run(20*ms)
pyr_group.Iinj_soma[0] = 130 * pA
run(200*ms)
pyr_group.Iinj_soma[0] = 0 * pA
run(120*ms)


fig, axes = plt.subplots(3,1, figsize = [5.4, 6])
ax1, ax2, ax3 = axes[0], axes[1], axes[2]

ax1.set_title("bAPs")
ax1.plot(M.t/ms, M.V_soma[0]/mV, label='soma' ,c='#005c94ff')
ax1.plot(M.t/ms, M.V_trunk[0]/mV, label='trunk', c='#338000ff')
ax1.plot(M.t/ms, M.V_prox[0]/mV, label='proximal', c='#ff6600ff')
ax1.plot(M.t/ms, M.V_dist[0]/mV, label='distal', c='#aa0044ff')
ax1.plot([0,350], [-40,-40], label='threshold', ls='--', c='grey')
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)
ax1.spines['left'].set_visible(False)
ax1.spines['bottom'].set_visible(False)
ax1.set_xticks([])
ax1.set_yticks([])
ax1.set_ylim(top = 40)

ax2.set_title("dspikes distal")
ax2.plot(M.t/ms, M.V_soma[1]/mV, label='soma' ,c='#005c94ff')
ax2.plot(M.t/ms, M.V_trunk[1]/mV, label='trunk', c='#338000ff')
ax2.plot(M.t/ms, M.V_prox[1]/mV, label='proximal', c='#ff6600ff')
ax2.plot(M.t/ms, M.V_dist[1]/mV, label='distal', c='#aa0044ff')
ax2.plot([0,350], [-40,-40], label='threshold', ls='--', c='grey')
ax2.plot(range(100, 100+(N*isi), isi), [20 for i in range(N)], "|", ms=4, c='#aa0044ff')
ax2.plot([20,40],[-10, -10], c='black')
ax2.plot([40,40],[-10, 10], c='black')
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
ax2.spines['left'].set_visible(False)
ax2.spines['bottom'].set_visible(False)
ax2.set_xticks([])
ax2.set_yticks([])
ax2.set_ylim(top = 40)

ax3.set_title("dspikes proximal")
ax3.plot(M.t/ms, M.V_soma[2]/mV, label='soma' ,c='#005c94ff')
ax3.plot(M.t/ms, M.V_trunk[2]/mV, label='trunk', c='#338000ff')
ax3.plot(M.t/ms, M.V_prox[2]/mV, label='proximal', c='#ff6600ff')
ax3.plot(M.t/ms, M.V_dist[2]/mV, label='distal', c='#aa0044ff')
ax3.plot([0,350], [-40,-40], label='threshold', ls='--', c='grey')
ax3.plot(range(100, 100+(N*isi), isi), [20 for i in range(N)], "|", ms=4, c='#ff6600ff')
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)
ax3.spines['left'].set_visible(False)
ax3.spines['bottom'].set_visible(False)
ax3.set_xticks([])
ax3.set_yticks([])
ax3.set_ylim(top = 40)
fig.tight_layout()

inlet1, ax4 = subplots(figsize = [3, 3])
ax4.set_title("bAPs (zoomed)")
ax4.plot(M.t/ms, M.V_soma[0]/mV, label='soma' ,c='#005c94ff')
ax4.plot(M.t/ms, M.V_trunk[0]/mV, label='trunk', c='#338000ff')
ax4.plot(M.t/ms, M.V_prox[0]/mV, label='proximal', c='#ff6600ff')
ax4.plot(M.t/ms, M.V_dist[0]/mV, label='distal', c='#aa0044ff')
ax4.plot([123,124],[-10, -10], c='black')
ax4.plot([124,124],[-10, 10], c='black')
ax4.spines['top'].set_visible(False)
ax4.spines['right'].set_visible(False)
ax4.spines['bottom'].set_visible(False)
ax4.spines['left'].set_visible(False)
ax4.set_xticks([])
ax4.set_yticks([])
ax4.set_xlim(120,125)
ax4.set_ylim(-60,45)
tight_layout()

inlet2, ax5 = subplots(figsize = [3, 3])
ax5.set_title("dspikes proximal (zoomed)")
ax5.plot(M.t/ms, M.V_soma[2]/mV, label='soma' ,c='#005c94ff')
ax5.plot(M.t/ms, M.V_trunk[2]/mV, label='trunk', c='#338000ff')
ax5.plot(M.t/ms, M.V_prox[2]/mV, label='proximal', c='#ff6600ff')
ax5.plot(M.t/ms, M.V_dist[2]/mV, label='distal', c='#aa0044ff')
ax5.plot([158,159],[-10, -10], c='black')
ax5.plot([159,159],[-10, 10], c='black')
ax5.spines['top'].set_visible(False)
ax5.spines['right'].set_visible(False)
ax5.spines['bottom'].set_visible(False)
ax5.spines['left'].set_visible(False)
ax5.set_xticks([])
ax5.set_yticks([])
ax5.set_xlim(155,160)
ax5.set_ylim(-60,45)
tight_layout()

plt.show()