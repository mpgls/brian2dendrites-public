from math import pi
from brian2.units import *
from brian2_dendrites import Properties


# General passive parameters:
sf       =  2.8                     # scale factor to account for reduced model area   
Cm_soma  =  1  * uF/(cm**2) * sf    # specific membrane capacitance
gL_soma  =  25 * uS/(cm**2) * sf    # specific leakage conductance  
Cm_dend  =  Cm_soma * 1.5           # to account for spines
gL_dend  =  gL_soma * 1.5           # to account for spines
Ra       =  150.0 * ohm * cm        # specific axial resistance

# Create some Properties objects
soma_params = Properties(length=30*um, diameter=30*um, cm=Cm_soma, gl=gL_soma, 
                         ra=Ra, Vrest=-70*mV, Vth=-40*mV, tag='soma')

trunk_params = Properties(length=100*um, diameter=2.5*um, cm=Cm_dend, gl=gL_dend, 
                          ra=Ra, Vrest=-70*mV, Vth=-40*mV, tag='trunk')

prox_params = Properties(length=150*um, diameter=2*um, cm=Cm_dend, gl=gL_dend, 
                         ra=Ra, Vrest=-70*mV, Vth=-40*mV, tag='prox')

dist_params = Properties(length=150*um, diameter=1*um, cm=Cm_dend, gl=gL_dend, 
                         ra=Ra, Vrest=-70*mV, Vth=-40*mV, tag='dist')

g_couples = {
"g_trunk_soma"   :  trunk_params.g_cylinder,    # Using the normal g_couple results in very strong coupling
"g_prox_trunk"   :  Properties.find_g_couple(trunk_params, prox_params),
"g_dist_prox"    :  Properties.find_g_couple(dist_params, prox_params)
}

general_synaptic_params = {
"Egaba"          :  -80   * mV,
"tGABA"          :   10   * ms,
"Eampa"          :   0    * mV,
"tAMPA"          :   7    * ms,
"Enmda"          :   0    * mV,
"tNMDA"          :   70   * ms, 
"gamma"          :   0.062 * mV**-1,
"Mg"             :   1,           
"eta"            :   0.1
}

synaptic_params = {
"gAMPA_region1_dist"  :   1  * nS,
"gNMDA_region1_dist"  :   1  * nS,
"gAMPA_region2_prox"  :   1  * nS,
"gNMDA_region2_prox"  :   1  * nS
}

dspike_params = {
"INa_refractory"   :  5  * ms,
"tau_Na"           : 0.6 * ms,     # INa decay constant
"tau_K"            : 0.6 * ms,     # IK decay constant
# Current jumps at dendritic spike events:
"INa_dist_max"    :   2900 * pA * (dist_params.area/prox_params.area),
"IK_dist_max"     :  -3000 * pA * (dist_params.area/prox_params.area),
"INa_prox_max"    :   2900 * pA,
"IK_prox_max"     :  -3000 * pA,
"INa_trunk_max"   :   2900 * pA * (trunk_params.area/prox_params.area),
"IK_trunk_max"    :  -3000 * pA * (trunk_params.area/prox_params.area)
}


pyramidal_params = Properties.merge_params([soma_params, trunk_params, prox_params, 
                             dist_params, g_couples, general_synaptic_params,
                             synaptic_params, dspike_params])