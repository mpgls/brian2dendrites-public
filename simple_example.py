from brian2 import *
from brian2_dendrites import Compartment, NeuronModel
from simple_example_params import pyramidal_params

# Create compartments
soma = Compartment('soma', model='leakyIF', neuron="pyramidal")
apical = Compartment('apical', model='passive', neuron="pyramidal")
basal = Compartment('basal', model='passive', neuron="pyramidal")

# Connect them
apical.connect_to(soma)
basal.connect_to(soma)

# Add noise currents
soma.add_noise()
apical.add_noise()
basal.add_noise()

# Merge compartments into a single model
pyr_model = NeuronModel(neuron='pyramidal', namespace=pyramidal_params)

# Create a Brian NeuronGroup
pyr_group = NeuronGroup(4, model=pyr_model.equations, threshold='V_soma > -40*mV', 
                        method='euler', reset='V_soma = -50*mV', refractory=3*ms, 
                        name='pyr_group', namespace=pyramidal_params)

pyr_model.bind(pyr_group, scope=globals())    # Creates a connection between a NeuronModel and a NeuronGroup that unlocks some automations
pyr_model.set_rest()    # Initializes resting potentials for all compartments

# Set activity monitors
M = StateMonitor(pyr_group, ["V_soma", "V_apical", "V_basal"], record=True)

# Run simulation
run(100*ms)
pyr_group.Iinj_soma[1] = 100 * pA
pyr_group.Iinj_apical[2] = 100 * pA
pyr_group.Iinj_basal[3] = 100 * pA
run(400*ms)
pyr_group.Iinj_soma[1] = 0 * pA
pyr_group.Iinj_apical[2] = 0 * pA
pyr_group.Iinj_basal[3] = 0 * pA
run(250*ms)

# sns.set()
fig, axes = plt.subplots(2,2, figsize = [5.5,3], sharex=True, sharey=True,)
ax1, ax2, ax3, ax4 = axes[0,0], axes[0,1], axes[1,0], axes[1,1]

ax1.plot(M.t/ms, M.V_soma[0]/mV, label='soma', c='#005c94ff')
ax1.plot(M.t/ms, M.V_apical[0]/mV, label='apical', c='#ff6600ff')
ax1.plot(M.t/ms, M.V_basal[0]/mV, label='basal', c='#338000ff')
ax1.set_title("noise only", fontsize=10)
ax1.spines['right'].set_visible(False)
ax1.spines['top'].set_visible(False)
ax1.set_ylabel("voltage (mV)", fontsize=11)

ax2.plot(M.t/ms, M.V_soma[1]/mV, label='soma', c='#005c94ff')
ax2.plot(M.t/ms, M.V_apical[1]/mV, label='apical', c='#ff6600ff')
ax2.plot(M.t/ms, M.V_basal[1]/mV, label='basal', c='#338000ff')
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)
ax2.set_title("100 pA \u2192 soma",fontsize=10)

ax3.plot(M.t/ms, M.V_soma[2]/mV, label='soma', c='#005c94ff')
ax3.plot(M.t/ms, M.V_apical[2]/mV, label='apical', c='#ff6600ff')
ax3.plot(M.t/ms, M.V_basal[2]/mV, label='basal', c='#338000ff')
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)
ax3.set_title("100 pA \u2192 apical", fontsize=10)
ax3.set_xlabel("time (ms)", fontsize=11)

ax4.plot(M.t/ms, M.V_soma[3]/mV, label='soma', c='#005c94ff')
ax4.plot(M.t/ms, M.V_apical[3]/mV, label='apical', c='#ff6600ff')
ax4.plot(M.t/ms, M.V_basal[3]/mV, label='basal', c='#338000ff')
ax4.spines['right'].set_visible(False)
ax4.spines['top'].set_visible(False)
ax4.set_title("100 pA \u2192 basal", fontsize=10)

plt.tight_layout()
plt.show()