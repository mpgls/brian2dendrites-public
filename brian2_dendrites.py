#!/usr/bin/env python
# -*- coding: utf-8 -*-
# conda version : 4.8.3
# conda-build version : 3.18.12
# python version : 3.7.6.final.0
# brian2 version : 2.3 (py37hc9558a2_0)

import sys
from math import pi

# A dictionary that contains templates of frequently used equations
library = {
    'adex': ('dV{0}/dt = (gL{0} * (EL{0}-V{0}) + gL{0}*DeltaT{0}*exp((V{0}-Vth{0})/DeltaT{0}) + I{0} - w{0}) / C{0}  :volt\n'
             'dw{0}/dt = (a{0} * (V{0}-EL{0}) -w{0}) / tauw{0}  :amp\n'
             'I{0} = Iinj{0}  :amp\n'
             'Iinj{0}  :amp'),

    'adaptiveIF': ('dV{0}/dt = (gL{0} * (EL{0}-V{0}) + I{0} - w{0}) / C{0}  :volt\n'
                   'dw{0}/dt = (a{0} * (V{0}-EL{0}) - w{0}) / tauw{0}  :amp\n'
                   'I{0} = Iinj{0}  :amp\n'
                   'Iinj{0}  :amp'),
    
    'leakyIF': ('dV{0}/dt = (gL{0} * (EL{0}-V{0}) + I{0}) / C{0}  :volt\n'
                'I{0} = Iinj{0}  :amp\n'
                'Iinj{0}  :amp'),

    'passive': ('dV{0}/dt = (gL{0} * (EL{0}-V{0}) + I{0}) / C{0}  :volt\n'
                'I{0} = Iinj{0}  :amp\n'
                'Iinj{0}  :amp'),

    'gaba': ('Igaba_{1}_{0} = gGABA_{1}_{0} * (Egaba-V_{0}) * s_gaba_{1}_{0}  :amp\n'
             'ds_gaba_{1}_{0}/dt = -s_gaba_{1}_{0} / tGABA  :1'),

    'ampa': ('Iampa_{1}_{0} = gAMPA_{1}_{0} * (Eampa-V_{0}) * s_ampa_{1}_{0}  :amp\n'
             'ds_ampa_{1}_{0}/dt = -s_ampa_{1}_{0} / tAMPA  :1'),

    'nmda': ('Inmda_{1}_{0} = gNMDA_{1}_{0} * (Enmda-V_{0}) * s_nmda_{1}_{0} / (1.0 + eta*Mg*exp(-gamma*V_{0}))  :amp\n'
             'ds_nmda_{1}_{0}/dt = -s_nmda_{1}_{0}/tNMDA  :1'),
    
    'run_on_events': ("run_on_event('activate_INa_{0}', 'INa_{0} += INa_{0}_max; allow_INa_{0}=False; allow_IK_{0}=True; timer_{0} = t') \n"
                      "run_on_event('activate_IK_{0}', 'IK_{0} += IK_{0}_max; allow_IK_{0}=False; allow_INa_{0}=True')")
    }  


class Compartment(object):
    """ 
    A class that automatically generates and handles all differential equations
    describing a single compartment and the currents (synaptic/dendritic/noise) 
    passing through it.

    ---------------------------------------------------------------------------
    * ATTRIBUTES (user-defined)
    
    tag: str
        A unique tag used to separate the various compartments belonging to a 
        single neuron. It is also used for the creation of compartment-specific
        equations.
   
    model: str
        A keyword for accessing elements of the library. Custom models can also
        be provided but they should be in the same formattable structure as the 
        library models. Inexperienced users should avoid using custom models.
    
    neuron: str
        A tag that helps to group compartments of the same neuron.
        
    ---------------------------------------------------------------------------
    * OTHER ATTRIBUTES (handled by class functions but accessible to the user)
    
    eqs: str
        Stores compartment-specific differential equations.
    
    events: dict
        Stores compartment-specific custom events.
    
    event_actions: str
        Executable code that specifies model behaviour during custom eventS.
    """
    
    neuron_types = {}    # Stores all neurons created and their respective compartments

    def __init__(self, tag=None, model=None, neuron=None):
        self.tag = tag
        self.model = model
        self.neuron = neuron
        self.events = None  
        self.event_actions = None
        # Choose a premaid model template or provide a custom one:
        if self.model in library:
            self.eqs = library[self.model].format('_'+self.tag)
        else:
            self.eqs = self.model.format('_'+self.tag)
        # Keep a record of every new compartment:
        if neuron not in self.neuron_types:
            self.neuron_types[neuron] = [self]
        else:
            self.neuron_types[neuron].append(self)                
        

    def __str__(self):
        """
        Prints useful information about a Compartment object.
        Usage -> print(object_name) 
        """
        attrs = self.__dict__
        details = ["\u2192 {}: \n{}\n".format(i, attrs[i]) for i in attrs]
        msg = ("OBJECT:\n{0}\n"
               "\n=======================================================\n\n"
               "ATTRIBUTES:\n{1}")
        return msg.format(self.__class__, "\n".join(details))

        
    def connect_to(self, other):
        """
        Allows the electrical coupling of two compartments.
        Usage -> object1.connect_to(object2)
        """
        # Create equations for forward and backpropagating dendritic currents:
        I_forward = 'I_{0}_{1} = (V_{0}-V_{1}) * g_{0}_{1}  :amp'.format(
                     self.tag, other.tag)
        I_backward = 'I_{1}_{0} = (V_{1}-V_{0}) * g_{0}_{1}  :amp'.format(
                     self.tag, other.tag)
        
        # Include them to their respective compartments:
        other.eqs += '\n'+I_forward
        self.eqs += '\n'+I_backward
        
        # Add them to the I variable (Iinj -> Inj + new_current):
        pre_change = '= Iinj_{0}'.format(self.tag)
        post_change = '= Iinj_{0}'.format(other.tag)
        self.eqs = self.eqs.replace(
            pre_change, pre_change+' + '+I_backward.split('=')[0])
        other.eqs = other.eqs.replace(
            post_change, post_change + ' + ' + I_forward.split('=')[0])


    def add_synapse(self, type=None, source=None):
        """
        Adds ampa/nmda/gaba synapses from a specified source. The 'source' 
        kwarg is used to separate synapses of the same type coming from 
        different sources. Usage -> object.add_synapse('type') 
        """
        current_name = 'I{0}_{1}_{2}'.format(type, source, self.tag)
        current_eqs = library[type].format(self.tag, source)
        to_replace = '= Iinj_{0}'.format(self.tag)
        self.eqs = self.eqs.replace(to_replace, 
                                    to_replace + ' + ' + current_name)
        self.eqs += '\n'+current_eqs


    def add_noise(self, tau='20*ms', sigma='3.5*pA', mean = '5*pA'):
        """
        Adds coloured noisy current. More info here:
        https://brian2.readthedocs.io/en/stable/user/models.html#noise
        Usage-> object.add_noise(optonal kwargs)
        """
        Inoise_name = 'I_noise_{0}'.format(self.tag)
        noise_eqs = 'dI_noise_{0}/dt = ({3}-I_noise_{0})/({1}) + {2}*(sqrt(2/({1}))*xi_{0}) :amp'.format(
                     self.tag, tau, sigma, mean)
        to_replace = '= Iinj_{0}'.format(self.tag)
        self.eqs = self.eqs.replace(to_replace, to_replace + ' + ' + Inoise_name)
        self.eqs += '\n'+noise_eqs
        
        
    def add_dspikes(self):
        """
        Adds dendritic spike currents (rise->INa, decay->IK) and some other 
        variables for controlling custom events.
        Usage-> object.add_dspikes()
        """
        # The following code creates all necessary equations for dspikes:
        dspike_currents = 'INa_{0} + IK_{0}'.format(self.tag)
        INa_eqs = 'dINa_{0}/dt = -INa_{0}/tau_Na  :amp'.format(self.tag)
        IK_eqs = 'dIK_{0}/dt = -IK_{0}/tau_K  :amp'.format(self.tag)
        INa_check = 'allow_INa_{0}  :boolean'.format(self.tag)
        IK_check = 'allow_IK_{0}  :boolean'.format(self.tag)
        refractory_var = 'timer_{0}  :second'.format(self.tag)
        to_replace = '= Iinj_{0}'.format(self.tag)
        self.eqs = self.eqs.replace(to_replace, to_replace + ' + ' + dspike_currents)
        self.eqs += '\n'.join(['', INa_eqs, IK_eqs, INa_check, 
                               IK_check, refractory_var])
        
        # Create all necessary custom events for dspikes:
        condition_INa = 'V_{0} > Vth_{0} and allow_INa_{0} and t > timer_{0} + INa_refractory'
        condition_IK = 'INa_{0} < (0.5*INa_{0}_max) and allow_IK_{0}'
        if self.events is None:
            self.events = {}
            self.events["activate_INa_"+self.tag] = condition_INa.format(self.tag)
            self.events["activate_IK_"+self.tag] = condition_IK.format(self.tag)
        
        # Specify what is going to happen inside run_on_event()
        self.event_actions = library['run_on_events'].format(self.tag)

            
class NeuronModel(object):
    """
    Merges multiple Compartment objects into a single model. It also contains 
    useful functions for custom event handling and parameter initialisation.
    
    ---------------------------------------------------------------------------
    * PARAMETERS (user-defined)     
    
    neuron: str
        Specifies which compartments to collect from Compartment.neuron_types.
        
    namespace: dict, optional
        A dictionary mapping identifier names to parameter objects.
    
    ---------------------------------------------------------------------------
    * PROPERTIES (handled by class functions but accessible to user)
    
    compartments: list
        A list of Compartment objects that are grouped by their neuron 
        attribute.
    
    linked_neurongroup: brian2.NeuronGroup
        Reference to a NeuronGroup object that is linked to a specified 
        NeuronModel onject.
    
    varscope: dict
        Used to allow access to global variables of the main simulation script.
    """
                                                                            
    def __init__(self, neuron=None, namespace=None):
        self.namespace = namespace
        self.compartments = Compartment.neuron_types[neuron]
        self.linked_neurongroup = None
        self.varscope = None
    

    def __str__(self):
        """
        Prints useful information about a NeuronModel object.
        Usage -> print(onjectName)
        """
        attrs = self.__dict__
        details = ["\u2192 {0}: \n{1}\n".format(i, attrs[i]) for i in attrs]
        msg = ("OBJECT:\n{0}\n"
               "\n=======================================================\n\n"
               "ATTRIBUTES:\n{1}"
               "\n=======================================================\n\n"
               "PROPERTIES:\n"
               "\u2192 events: \n{2}\n\n"
               "\u2192 equations: \n{3}")
        return msg.format(self.__class__, "\n".join(details), 
                          self.events, self.equations)


    @property
    def equations(self):
        """
        Merges all compartment equations into a single string
        """
        all_eqs = [i.eqs for i in self.compartments]
        return '\n\n'.join(all_eqs)
    
    
    @property
    def events(self):
        """
        Creates a dict of all custom events
        """
        d_out = {}
        all_events = [i.events for i in self.compartments if i.events]
        for d in all_events:
            d_out.update(d)
        return d_out
    
    
    def bind(self, ng, scope={}):
        """
        Used to create a link between a NeuronModel and its corresponding
        NeuronGroup object. Unlocks set_rest and handle_dspikes methods
        """
        if scope == {}:
            print("Error: Please include <scope=globals()> inside"
                  "the bind() method before calling it.")
            sys.exit()
        else:
            self.linked_neurongroup = ng
            self.varscope = scope
        
        
    def set_rest(self, debug=False):
        """
        Creates and runs executable code that initialises V rest across
        all NeuronModel compartments. Setting debug=True just prints it.
        """
        command = '{0}.V_{1} = {2}'
        if not self.namespace:    # When namespace is not specified in NeuronGroupg
            commands = [command.format(self.linked_neurongroup.name,
                        i.tag,'EL_'+i.tag) for i in self.compartments]
        elif self.namespace:    # When model parameters are passed as dict to the NeuronGroup
            commands = [command.format(self.linked_neurongroup.name, 
                        i.tag, repr(self.namespace['EL_'+i.tag])) 
                        for i in self.compartments]
        executable = '\n'.join(commands)
        if debug == True:
            print(executable)
        else:
            exec(executable, self.varscope)
        

    def handle_dspikes(self, debug=False):
        """
        Creates and runs executable code that: 
        a) Initialises custom event checkpoint variables.
        b) Specifies what happens during custom events.
        Setting debug=True just prints all the code.
        """
        ng = self.linked_neurongroup.name
        active_branches = [i for i in self.compartments if i.events]
        checks = ('{0}.allow_INa_{1} = True \n'
                  '{0}.allow_IK_{1} = False')
        all_checks = [checks.format(ng, i.tag) for i in active_branches]  
        all_actions = [i.event_actions for i in active_branches]
        commands = '\n'.join(all_checks + all_actions)
        executable = commands.replace('run_on_event', ng+'.run_on_event')
        if debug == True:
            print(executable)
        else:
            exec(executable, self.varscope)


class Properties(object):
    """
    WARNING: This class is under development. Some of its features might change 
    or become part of the Compartment & Neuromodel classes.
    
    A class that helps to declare all necessary model parameters.
    """
    
    def __init__(self, length=None, diameter=None, cm=None, gl=None, ra=None,
                 Vrest=None, Vth=None, tag=None):
        self.length = length
        self.diameter = diameter
        self.cm = cm
        self.gl = gl
        self.ra = ra
        self.Vrest = Vrest
        self.Vth = Vth
        self.tag = tag


    @property
    def area(self):
        """
        Returns the surface area a of a compartment (open cylinder) based 
        on its length and diameter. 
        """
        return pi * self.length * self.diameter


    @property
    def capacitance(self):
        """
        Returns the absolute membrane capacitance of a single compartment 
        based on its surface area and its specific membrane capacitance.
        """
        return self.area * self.cm


    @property
    def g_leakage(self):
        """
        Returns the absolute leakage conductance of a single compartment based 
        on its surface area and its specific leakage conductance.
        """
        return self.area * self.gl


    @property
    def params(self):
        d = {"EL_"+self.tag: self.Vrest,
             "C_"+self.tag: self.capacitance,
             "gL_"+self.tag: self.g_leakage,
             "Vth_"+self.tag: self.Vth}
        return d


    @property
    def g_cylinder(self):
        """
        The conductance (of coupling currents) passing through a cylindrical
        compartment based on its dimensions and its axial resistance. To be 
        used when then the total number of compartments is low and the 
        adjacent-to-soma compartments are highly coupled with the soma.
        """
        ri = (4*self.ra*self.length) / (pi*self.diameter**2)
        return 1/ri


    @staticmethod
    def find_g_couple(comp1, comp2):
        """
        The conductance (of coupling currents) between the centers of 
        two adjacent cylindrical compartments, based on their dimensions
        and the axial resistance.
        """
        r1 = (4 * comp1.ra * comp1.length) / (pi * comp1.diameter**2)
        r2 = (4 * comp2.ra * comp2.length) / (pi * comp2.diameter**2)
        ri = (r1+r2) / 2
        return 1/ri


    @classmethod
    def merge_params(cls, params_list):
        d_out = {}
        for d in params_list:
            if isinstance(d, cls):
                d_out.update(d.params)
            else:
                d_out.update(d)
        return d_out